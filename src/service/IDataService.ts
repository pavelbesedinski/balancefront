export default interface IDataService<T, M> {
  getData: (value: T, method: M) => Promise<T>;
}
