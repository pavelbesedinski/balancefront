import BalanceInput from "../components/model/balanceInput/BalanceInput";
import { URL } from "../configuration/conf";
import IDataService from "./IDataService";
import Service from "./Service";

export default class DataService extends Service
  implements IDataService<BalanceInput, string> {
  public async getData(data: BalanceInput, solver: string) {
    try {
      const response = await fetch(`${URL}${this.targetURL}`, {
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
          "Solver": solver,
        },
        method: "POST",
      });
      if (!response.ok) {
        throw new Error(response.statusText);
      }
      return response.json();
    } catch (err) {
      throw err;
    }
  }
}
