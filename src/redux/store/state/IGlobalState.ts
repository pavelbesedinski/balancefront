import BalanceOutput from "../../../components/model/balanceOutput/BalanceOutput" ;

export interface IGlobalState {
  allocateData: BalanceOutput;
  selectMethod: string;
}
