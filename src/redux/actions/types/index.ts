import BalanceOutput from "../../../components/model/balanceOutput/BalanceOutput";
import * as Constants from "../../constants";
import { IAction } from "./IAction";

export type AllocateData = IAction<typeof Constants.ALLOCATE_DATA, BalanceOutput>;

export type SelectMethod = IAction<typeof Constants.SELECT_METHOD, string>;
