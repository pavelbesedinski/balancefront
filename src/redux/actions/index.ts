import BalanceOutput from "../../components/model/balanceOutput/BalanceOutput";
import * as Constants from "../constants";
import * as actionTypes from "./types";

export const allocateData = (
  value: BalanceOutput
): actionTypes.AllocateData => ({
  payload: value,
  type: Constants.ALLOCATE_DATA,
});

export const selectMethod = (value: string): actionTypes.SelectMethod => ({
  payload: value,
  type: Constants.SELECT_METHOD,
});
