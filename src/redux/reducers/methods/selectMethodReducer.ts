import { SOLVERS } from "../../../genconstants/constants";
import * as actionTypes from "../../actions/types";
import * as Constants from "../../constants";

const initialState = SOLVERS[0];

export const SelectMethodReducer = (
  state: string = initialState,
  action: actionTypes.SelectMethod
): string => {
  switch (action.type) {
    case Constants.SELECT_METHOD: {
      return action.payload;
    }
    default: {
      return state;
    }
  }
};
