import BalanceOutput from "../../../components/model/balanceOutput/BalanceOutput";
import * as actionTypes from "../../actions/types";
import * as Constants from "../../constants";

const initialState = null;

export const AllocateDataReducer = (
  state: BalanceOutput | null = initialState,
  action: actionTypes.AllocateData
): BalanceOutput | null => {
  switch (action.type) {
    case Constants.ALLOCATE_DATA: {
      return action.payload;
    }
    default: {
      return state;
    }
  }
};
