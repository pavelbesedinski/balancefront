import { combineReducers } from "redux";

import { AllocateDataReducer } from "./methods/allocateDataReducer";
import { SelectMethodReducer } from "./methods/selectMethodReducer";

export const allReducers = combineReducers({
  allocateData: AllocateDataReducer,
  selectMethod: SelectMethodReducer,
});
