import BalanceOutput from "../components/model/balanceOutput/BalanceOutput";
import CytographModel from "../components/model/CytographModel/Graph";
import CytographEdges from "../components/model/CytographModel/GraphEdges";
import CytographNodes from "../components/model/CytographModel/GraphNodes";
import GraphModel from "../components/model/Graph/Graph";
import GraphEdges from "../components/model/Graph/GraphEdges";
import GraphNodes from "../components/model/Graph/GraphNodes";


export default class Utils {

  public transformCytrograph = (balanceOutput: BalanceOutput) => {
    const graphModel: CytographModel = new CytographModel();
    const sizeThreads = balanceOutput.balanceOutputVariables.length;

    let nodes: string[] = [];
    let counter: number = 0;

    for (let i = 0; i < sizeThreads; i++) {
      graphModel.edges[i] = new CytographEdges();
      const graphEdge = graphModel.edges[i].data;
      const balanceVariables = balanceOutput.balanceOutputVariables[i];

      if (balanceVariables.source === null) {
        graphEdge.source = `${"edgein" + counter++}`;
      } else {
        graphEdge.source = balanceVariables.source;
      }
      if (balanceVariables.target === null) {
        graphEdge.target = `${"edgeout" + counter++}`;
      } else {
        graphEdge.target = balanceVariables.target;
      }
      nodes[i * 2] = graphEdge.source;
      nodes[i * 2 + 1] = graphEdge.target;
      
      graphEdge.label = balanceVariables.value?this.MyRound(balanceVariables.value, 3).toString():"empty";
    }

    nodes = nodes.filter((item, pos) => {
      return nodes.indexOf(item) === pos;
    });

    for (let i = 0; i < nodes.length; i++) {
      graphModel.nodes[i] = new CytographNodes();
      const graphNode = graphModel.nodes[i].data;

      graphNode.id = nodes[i];
      graphNode.label = nodes[i].includes("edgeout")?"OUT":nodes[i].includes("edgein")?"IN":nodes[i].toString();
    }

    return graphModel;
  };

  public transformSigma = (balanceOutput: BalanceOutput) => {
    const graphModel: GraphModel = new GraphModel();
    const sizeThreads = balanceOutput.balanceOutputVariables.length;

    let nodes: string[] = [];
    let counter: number = 0;

    for (let i = 0; i < sizeThreads; i++) {
      graphModel.edges[i] = new GraphEdges();
      const graphEdge = graphModel.edges[i];
      const balanceVariables = balanceOutput.balanceOutputVariables[i];

      graphEdge.id = balanceVariables.id.toString();
      graphEdge.label = this.MyRound(balanceVariables.value, 3).toString();

      if (balanceVariables.source === null) {
        graphEdge.source = `${"edge" + counter++}`;
      } else {
        graphEdge.source = balanceVariables.source.toString();
      }
      if (balanceVariables.target === null) {
        graphEdge.target = `${"edge" + counter++}`;
      } else {
        graphEdge.target = balanceVariables.target.toString();
      }

      nodes[i * 2] = graphEdge.source;
      nodes[i * 2 + 1] = graphEdge.target;
    }
    nodes = nodes.filter((item, pos) => {
      return nodes.indexOf(item) === pos;
    });

    for (let i = 0; i < nodes.length; i++) {
      graphModel.nodes[i] = new GraphNodes();
      const graphNode = graphModel.nodes[i];

      graphNode.id = nodes[i];
      graphNode.label = i.toString();
    }

    return graphModel;
  };
  private MyRound = (value: number, precision: number) => {
    const multiplier = Math.pow(10, precision || 0);
    return Math.round(value * multiplier) / multiplier;
  };
}
