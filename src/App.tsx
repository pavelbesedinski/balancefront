import * as React from "react";

import Container from "src/components/container/Page";
import Content from "./components/parts/Content";
import { POST_KEY } from "./configuration/conf";
import DataService from "./service/DataService";

export default class App extends React.Component {
  public render() {
    return (
      <Container>
        <Content service={new DataService(POST_KEY)} />
      </Container>
    );
  }
}
