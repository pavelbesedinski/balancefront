export const NOT_FOUND = 404;
export const SEND_DATA_BUTTON_NAME = "Отправить";
export const INPUT_FORM_NAME = "JSON";
export const SOLVERS = ["JOptimizer", "OjAlgo", "MATLAB", "Gurobi"];
export const EMPTY = "EMPTY";
export const IN = "IN";
export const OUT = "OUT";
export const INFEASIBLE = "ENFEASIBLE";