import BalanceConstraints from "../balanceInput/BalanceConstraints";

export default class BalanceInputVariables {
    public id: string = "";
    public sourceId:  string = "";
    public destinationId:  string = "";
    public name: string = "";
    public measured: number = 0;
    public metrologicRange: BalanceConstraints = new BalanceConstraints();
    public technologicRange: BalanceConstraints = new BalanceConstraints();
    public tolerance: number = 0;
    public isMeasured: boolean = true;
    public isExcluded: boolean = false;
  }
  