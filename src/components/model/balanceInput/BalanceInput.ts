import BalanceInputVariables from "../balanceInput/BalanceInputVariables";
import BalanceSettings from "../balanceInput/BalanceSettings";

export default class BalanceInput {
  public balanceInputVariables: BalanceInputVariables[] = [];
  public balanceSettings: BalanceSettings = new BalanceSettings();
}
