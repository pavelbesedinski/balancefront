export default class BalanceSettings {
  public roundUnit: number = 0;
  public balanceSettingsConstraints: string = "METROLOGIC";
}
