import GraphEdges from "./GraphEdges";
import GraphNodes from "./GraphNodes";

export default class Graph {
  public nodes: GraphNodes[] = [];
  public edges: GraphEdges[] = [];
}
