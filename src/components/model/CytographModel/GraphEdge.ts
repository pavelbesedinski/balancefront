export default class GraphEdge {
  public source: string = "";
  public target: string = "";
  public label: string = "";
}
