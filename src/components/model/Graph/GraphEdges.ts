export default class GraphEdges {
  public id: string = "";
  public source: string = "";
  public target: string = "";
  public label: string = "";
}
