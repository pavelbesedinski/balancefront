export default class BalanceOutputVariables {
  public id:  string = "";
  public name: string = "";
  public source:  string = "";
  public target:  string = "";
  public value: number = 0;
}
