import BalanceOutputVariables from "../balanceOutput/BalanceOutputVariables";

export default class BalanceOutput {
  public balanceOutputVariables: BalanceOutputVariables[] = [];
  public imbalance: number = 0;
  public solverMode: string = "";
  public solvingTime: number = 0.0;
  public preparingTime: number = 0.0;
}
