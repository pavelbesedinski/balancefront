import * as React from "react";

import Header from "../parts/header/Header";

interface IProps {
    children: JSX.Element
}

export default class ContentContainer extends React.Component<IProps> {
  public render() {
    const { children } = this.props;
    return (
      <div>
        <Header/>
        {children}
      </div>
    );
  }
}
