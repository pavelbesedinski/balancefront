import * as React from "react";

import BalanceOutputVariables from "src/components/model/balanceOutput/BalanceOutputVariables";
import { EMPTY, IN, OUT } from "src/genconstants/constants";

interface IProps {
  data: BalanceOutputVariables;
}

export default class TableHeader extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props);
  }
  public render() {
    let { source, target, name } = this.props.data;
    const {value} = this.props.data;
    name = (name == null || name.length === 0)? EMPTY : name;
    source = (source == null || source.length === 0)? IN : source;
    target = (target == null || target.length === 0)? OUT : target;
    return (
      <tr>
        <td>{name}</td>
        <td>{source}</td>
        <td>{target}</td>
        <td>{value}</td>
      </tr>
    );
  }
}
