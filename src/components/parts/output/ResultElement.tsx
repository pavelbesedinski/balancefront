import * as React from "react";

import BalanceOutput from "src/components/model/balanceOutput/BalanceOutput";
import BalanceOutputVariables from "src/components/model/balanceOutput/BalanceOutputVariables";
import { BalanceOutputVariablesKeyType } from "../tableTypes";
import TableHeader from "./TableHeader";
import TableRow from "./TableRow";

interface IProps {
  data: BalanceOutput | null;
}

export default class ResultElement extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props);
  }

  public render() {
    const { data } = this.props;

    const BalanceOutputVariablesArray: BalanceOutputVariablesKeyType[] = [
      "Name",
      "Source",
      "Target",
      "Value",
    ];

    return (
      <div className="table-responsive-lg">
        <table className="table table-borderless">
          <thead>
            <tr>
              {BalanceOutputVariablesArray.map((name) => (
                <TableHeader key={name} name={name} />
              ))}
            </tr>
          </thead>
          <tbody>
            {data?.balanceOutputVariables.map(
              (vals: BalanceOutputVariables) => (
                <TableRow data={vals} />
              )
            )}
          </tbody>
        </table>
      </div>
    );
  }
}
