import * as React from "react";
import { EMPTY } from 'src/genconstants/constants';

interface IProps {
  name: string;
}

export default class TableHeader extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props);
  }
  public render() {
    let { name } = this.props;
    name = name.length === 0? EMPTY: name;
    return (
      <th
        className="unselectable col-name"
        scope="col"
        style={{ cursor: "pointer" }}
      >
        <div>{name}</div>
      </th>
    );
  }
}
