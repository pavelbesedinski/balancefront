import * as React from "react";
import { connect } from "react-redux";

import { IGlobalState } from "src/redux/store/state/IGlobalState";

import BalanceOutput from "src/components/model/balanceOutput/BalanceOutput";
import { BalanceOutputKeyType } from "../tableTypes";
import ResultElement from "./ResultElement";

interface IProps {
  data: BalanceOutput | null;
}

interface IState {
  data: BalanceOutput | null;
}

class ContentList extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = { data: new BalanceOutput() };
  }

  public render() {
    const BalanceOutputArray: BalanceOutputKeyType[] = [
      "Imbalance",
      "Solver",
      "Time",
    ];
    const { data } = this.props;
    const time = data ? data.preparingTime + data.solvingTime : 0.0;

    const Element = () => {
      return (
        <div
          className="card w-75"
          style={{ marginBottom: "5px", marginTop: "5px", marginLeft: "10%" }}
        >
          <div className="card-header">Results: </div>
          <div className="card-body">
            <ResultElement data={data} />
          </div>
        </div>
      );
    };

    return (
      <div style={{ visibility: data !== null ? "visible" : "hidden" }}>
        <div
          className="card w-75"
          style={{ marginLeft: "10%", marginTop: "5px" }}
        >
          <dt className="col-sm-2">{BalanceOutputArray[1]}:</dt>
          <dd className="col-sm-9">{data?.solverMode}</dd>
          <dt className="col-sm-2">{BalanceOutputArray[0]}:</dt>
          <dd className="col-sm-9">
            <td>{data?.imbalance}</td>
          </dd>
          <dt className="col-sm-2">{BalanceOutputArray[2]}:</dt>
          <dd className="col-sm-9">{time}</dd>
          <dt className="col-sm-2">Preparing Time:</dt>
          <dd className="col-sm-9">{data?.preparingTime}</dd>
          <dt className="col-sm-2">Solving Time:</dt>
          <dd className="col-sm-9">{data?.solvingTime}</dd>
        </div>
        {Element()}
      </div>
    );
  }
}

const mapStateToProps = (state: IGlobalState) => ({
  data: state.allocateData,
});

export default connect(mapStateToProps)(ContentList);
