import * as React from "react";
import { connect } from "react-redux";
import { bindActionCreators, Dispatch } from "redux";
import { IGlobalState } from "../../redux/store/state/IGlobalState";

import DataService from "src/service/DataService";
import Utils from "src/utils/Utils";
import GraphComponent from "../graph/Cytograph";
import ErrorComponent from "../graph/Error";
import BalanceOutput from "../model/balanceOutput/BalanceOutput";
import GraphModel from "../model/CytographModel/Graph";
import InputComponent from "./input/Input";
import MethodButtons from "./input/MethodButtons";
import SendDataButton from "./input/SendButton";
import ContentList from "./output/ContentList";

import {
  INPUT_FORM_NAME,
  SEND_DATA_BUTTON_NAME,
  SOLVERS,
} from "../../genconstants/constants";

import { allocateData, selectMethod } from "../../redux/actions";

interface IProps {
  service: DataService;
  solver: string;
  allocateData: typeof allocateData;
}

interface IState {
  data: BalanceOutput | null;
  graph: GraphModel | null;
  input: string;
  solver: string;
  loading: boolean;
  error: boolean;
}

class Content extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    const data = new BalanceOutput();
    const graph = null;
    const input = "";
    const solver = SOLVERS[0];
    const loading = false;
    this.state = { data, graph, input, solver, loading, error: false };
  }

  public handleButton = async () => {
    this.setState({ loading: true });
    try {
      const data = await this.props.service.getData(
        JSON.parse(this.state.input),
        this.props.solver
      );
      const graph = new Utils().transformCytrograph(data);
      this.setState({ data, graph, error: false });
      this.props.allocateData(data);
    } catch (err) {
      this.setState({ data: null, error: true });
    } finally {
      this.setState({ loading: false });
    }
  };

  public handleInput = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    this.setState({ input: event.target.value });
  };

  public handleSolver = (solver: string) => {
    this.setState({ solver });
  };

  public render() {
    const Entity = this.state.error ? (
      <ErrorComponent />
    ) : (
      <div>
        <div style={{ border: "1px solid Black", marginTop: "37px" }}>
          {
            <GraphComponent
              key={this.state.data?.imbalance}
              graph={this.state.graph}
            />
          }
        </div>
        <ContentList />
      </div>
    );
    return (
      <div>
        <div
          className="container-fluid"
          style={{ backgroundColor: "#e9ecef40" }}
        >
          <div className="row">
            <nav className="col-md-2 d-md-block" style={{ height: "100vh" }}>
              <InputComponent
                name={INPUT_FORM_NAME}
                value={this.state.input}
                handleChange={this.handleInput}
              />
              <MethodButtons solvers={SOLVERS} />
              <SendDataButton
                name={SEND_DATA_BUTTON_NAME}
                handleButton={this.handleButton}
                loading={this.state.loading}
              />
            </nav>

            <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-4">
              {Entity}
            </main>
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators({ allocateData, selectMethod }, dispatch);

const mapStateToProps = (state: IGlobalState) => ({
  solver: state.selectMethod,
});

export default connect(mapStateToProps, mapDispatchToProps)(Content);
