import * as React from "react";
import Spinner from './Spinner';

interface IProps {
  name: string;
  loading: boolean;
  handleButton(event: React.MouseEvent<HTMLButtonElement, MouseEvent>): void;
}

export default class SendButton extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props);
  }

  public render() {
    const content = this.props.loading? <Spinner/>:this.props.name;
    return (
      <button
        className="btn btn-lg btn-dark btn-block"
        type="button"
        onClick={this.props.handleButton}
        disabled={this.props.loading}
      >
        {content}
      </button>
    );
  }
}
