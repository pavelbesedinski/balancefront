import * as React from "react";

export default class Spinner extends React.Component {
  public render() {
    return (
      <div>
        <span
          className="spinner-border spinner-border-sm"
          role="status"
          aria-hidden="true"
          style = {{marginRight: "5px", marginBottom: "5px"}}
        />
        Загрузка...
      </div>
    );
  }
}
