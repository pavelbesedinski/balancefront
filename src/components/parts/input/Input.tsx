import * as React from "react";

interface IProps {
  name: string;
  value: string;
  handleChange(event: React.ChangeEvent<HTMLTextAreaElement>): void;
}

export default class Input extends React.Component<IProps> {
  constructor(props: IProps) {
    super(props);
  }
  public render() {
    return (
      <div className="form-group text-center" style={{ marginTop: "5px"}}>
        <label>{this.props.name}</label>
        <textarea
          className="form-control"
          id="exampleFormControlTextarea1"
          rows={3}
          style={{ height: "50vh" }}
          value={this.props.value}
          onChange={this.props.handleChange}
        />
      </div>
    );
  }
}
