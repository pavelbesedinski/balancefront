import * as React from "react";
import { connect } from "react-redux";
import { bindActionCreators, Dispatch } from "redux";

import { selectMethod } from "../../../redux/actions";

interface IState {
  solvers: string[];
  name: string;
}

interface IProps {
  solvers: string[];
  selectMethod: typeof selectMethod;
  // method(value: string): void;
}

class MethodButtons extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    const solvers = props.solvers;
    this.state = { solvers, name: solvers[0] };
  }

  public handleName1 = () => {
    this.setState({ name: this.state.solvers[0] });
    this.props.selectMethod(this.state.solvers[0]);
    // this.props.method(this.state.name);
  };
  public handleName2 = () => {
    this.setState({ name: this.state.solvers[1] });
    this.props.selectMethod(this.state.solvers[1]);

    // this.props.method(this.state.name);
  };
  public handleName3 = () => {
    this.setState({ name: this.state.solvers[2] });
    this.props.selectMethod(this.state.solvers[2]);

    // this.props.method(this.state.name);
  };
  public handleName4 = () => {
    this.setState({ name: this.state.solvers[3] });
    this.props.selectMethod(this.state.solvers[3]);

    // this.props.method(this.state.name);
  };

  public render() {
    return (
      <div className="dropdown" style={{ marginBottom: "5px" }}>
        <button
          className="btn btn-dark dropdown-toggle btn-block"
          type="button"
          id="dropdownMenuButton"
          data-toggle="dropdown"
          aria-haspopup="true"
          aria-expanded="false"
        >
          {this.state.name}
        </button>
        <div
          className="dropdown-menu"
          aria-labelledby="dropdownMenuButton"
          style={{ width: "100%", marginBottom: "5px" }}
        >
          <a className="dropdown-item" href="#" onClick={this.handleName1}>
            {this.state.solvers[0]}
          </a>
          <a className="dropdown-item" href="#" onClick={this.handleName2}>
            {this.state.solvers[1]}
          </a>
          <a className="dropdown-item" href="#" onClick={this.handleName3}>
            {this.state.solvers[2]}
          </a>
          <a className="dropdown-item" href="#" onClick={this.handleName4}>
            {this.state.solvers[3]}
          </a>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators({selectMethod}, dispatch);

export default connect(null, mapDispatchToProps)(MethodButtons);
