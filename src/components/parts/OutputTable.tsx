import * as React from "react";
import { connect } from "react-redux";

import { IGlobalState } from "../../redux/store/state/IGlobalState";

interface IProps {
  data: string;
}

interface IState {
  data: string;
}

class OutputTable extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    const { data } = props;
    this.state = { data };
  }
  public render() {
    return (
      <div key = {this.props.data} className="form-group text-center" style={{ marginTop: "5px" }}>
        {this.props.data}
      </div>
    );
  }
}

const mapStateToProps = (state: IGlobalState) => ({
  data: state.allocateData.solverMode,
});

export default connect(mapStateToProps)(OutputTable);
