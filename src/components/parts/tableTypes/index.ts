export type BalanceOutputVariablesKeyType =
  | "Name"
  | "Value"
  | "Source"
  | "Target";
export type BalanceOutputKeyType = "Imbalance" | "Solver" | "Time";
