import * as React from "react";

import Graph from "../model/Graph/Graph";
import GraphNodes from "../model/CytographModel/GraphNodes";
import GraphNode from "../model/CytographModel/GraphNode";
import GraphEdges from "../model/CytographModel/GraphEdges";
import GraphEdge from "../model/CytographModel/GraphEdge";
import CytoscapeComponent from "react-cytoscapejs";

export default class Cytograph extends React.Component {
  render() {
    const layout = { name: "cose" };

    const mgraph =
      this.props.graph !== null
        ? CytoscapeComponent.normalizeElements(
            JSON.parse(JSON.stringify(this.props.graph))
          )
        : [];

    return (
      <div>
        <div key={this.props.graph}>
          <CytoscapeComponent
            elements={mgraph}
            style={{ width: "100%", height: "50vh" }}
            layout={layout}
            stylesheet={[
              {
                selector: "node",
                style: {
                  width: 13,
                  height: 13,
                  shape: "circle",
                  label: "data(label)",
                  fontSize: "6px",
                },
              },
              {
                selector: "edge",
                style: {
                  width: 3,
                  shape: "triangle",
                  label: "data(label)",
                  fontSize: "6px",
                  "curve-style": "bezier",
                  "line-color": "#ccc",
                  "target-arrow-color": "#ccc",
                  "target-arrow-shape": "triangle",
                },
              },
            ]}
          />
        </div>
      </div>
    );
  }
}
