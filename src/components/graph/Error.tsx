import * as React from "react";

import { INFEASIBLE } from "../../genconstants/constants";
import BearPic from "../../images/bear.png";

export default class Error extends React.Component {
  public render() {
    return (
      <div className="container text-center">
        <h1>Ops... Error!</h1>
        <h2>{INFEASIBLE}</h2>
        <div className="col-md-1">
          <img className="rounded mx-auto d-block" src={BearPic} />
        </div>
      </div>
    );
  }
}
