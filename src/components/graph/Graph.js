import * as React from "react";

import { Sigma, RandomizeNodePositions, RelativeSize, EdgeShapes } from "react-sigma";
import { RESULT_VALUE } from "../../configuration/conf";
import { throws } from "assert";
import GraphModel from "../model/Graph/Graph";
import GraphEdge from "../model/Graph/GraphEdges";
import GraphNode from "../model/Graph/GraphNodes";

let myGraph = {
  nodes: [
    { id: "1", label: "Alice" },
    { id: "2", label: "Rabbit" },
    { id: "3", label: "Third" },
    { id: "4", label: "-1" },
  ],
  edges: [
    { id: "1", source: "1", target: "2", label: "SEES" },
    { id: "2", source: "2", target: "3", label: "SEES2" },
    { id: "3", source: "3", target: "4", label: "SEES2" },
  ],
};

const getGraph = () => {
  const graph = new GraphModel();
  graph.nodes[0] = new GraphNode();
  graph.nodes[0].id = "edge0";
  graph.nodes[0].label = "FAKE";
  graph.nodes[1] = new GraphNode();
  graph.nodes[1].id = "1";
  graph.nodes[1].label = "1";
  graph.nodes[2] = new GraphNode();
  graph.nodes[2].id = "edge1";
  graph.nodes[2].label = "2";
  graph.nodes[3] = new GraphNode();
  graph.nodes[3].id = "2";
  graph.nodes[3].label = "3";
  graph.nodes[4] = new GraphNode();
  graph.nodes[4].id = "edge2";
  graph.nodes[4].label = "4";
  graph.nodes[5] = new GraphNode();
  graph.nodes[5].id = "3";
  graph.nodes[5].label = "5";
  graph.nodes[6] = new GraphNode();
  graph.nodes[6].id = "edge3";
  graph.nodes[6].label = "6";
  graph.nodes[7] = new GraphNode();
  graph.nodes[7].id = "edge4";
  graph.nodes[7].label = "7";

  graph.edges[0] = new GraphEdge();
  graph.edges[0].id = "1";
  graph.edges[0].label = "1";
  graph.edges[0].source = "edge0";
  graph.edges[0].target = "1";
  graph.edges[1] = new GraphEdge();
  graph.edges[1].id = "2";
  graph.edges[1].label = "2";
  graph.edges[1].source = "1";
  graph.edges[1].target = "edge1";
  graph.edges[2] = new GraphEdge();
  graph.edges[2].id = "3";
  graph.edges[2].label = "3";
  graph.edges[2].source = "1";
  graph.edges[2].target = "2";
  graph.edges[3] = new GraphEdge();
  graph.edges[3].id = "4";
  graph.edges[3].label = "4";
  graph.edges[3].source = "2";
  graph.edges[3].target = "edge2";
  graph.edges[4] = new GraphEdge();
  graph.edges[4].id = "5";
  graph.edges[4].label = "5";
  graph.edges[4].source = "2";
  graph.edges[4].target = "3";
  graph.edges[5] = new GraphEdge();
  graph.edges[5].id = "6";
  graph.edges[5].label = "6";
  graph.edges[5].source = "3";
  graph.edges[5].target = "edge3";
  graph.edges[6] = new GraphEdge();
  graph.edges[6].id = "7";
  graph.edges[6].label = "7";
  graph.edges[6].source = "3";
  graph.edges[6].target = "edge4";

  return graph;
};

export default class Graph extends React.Component {
  render() {
    // JSON.parse(JSON.stringify(this.props.graph))
    const mgraph = this.props.graph !== null ? this.props.graph : getGraph();
    return (
      <div key={this.props.graph}>
        <Sigma
          renderer="canvas"
          style={{ width: "100%", height: "50vh" }}
          graph={mgraph}
          settings={{ drawEdges: true, clone: false, labelThreshold: 3 }}
          autoRescale = {true}
        >
          <EdgeShapes default="arrow" />
          <RelativeSize initialSize={15} />
          <RandomizeNodePositions />
        </Sigma>
      </div>
    );
  }
}
